# SOME DESCRIPTIVE TITLE.
# Copyright (C) : This page is licensed under a CC-BY-SA 4.0 Int. License
# This file is distributed under the same license as the Blender 3.5 Manual
# package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Blender 3.5 Manual 3.5\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-05 11:37+0900\n"
"PO-Revision-Date: 2025-02-24 16:57+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Greek <https://translate.blender.org/projects/blender-manual/"
"manual-web-ui/el/>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.10.1\n"
"Generated-By: Babel 2.10.3\n"

#: ../../build_files/templates/404.html:2
#: ../../build_files/templates/404.html:9
msgid "Not Found (404)"
msgstr ""

#: ../../build_files/templates/404.html:10
msgid "It seems the page you are trying to find has been either moved or deleted."
msgstr ""

#: ../../build_files/templates/404.html:11
msgid "You can try one of the following things:"
msgstr ""

#: ../../build_files/templates/404.html:15
msgid "Search"
msgstr ""

#: ../../build_files/templates/404.html:27
#: ../../build_files/templates/404.html:28
msgid "Return Home"
msgstr ""

#: ../../build_files/templates/components/footer_contribute.html:7
msgid "View Source"
msgstr ""

#: ../../build_files/templates/components/footer_contribute.html:11
msgid "View Translation"
msgstr ""

#: ../../build_files/templates/components/footer_contribute.html:23
msgid "Report issue on this page"
msgstr ""

#~ msgid "Edit Page"
#~ msgstr ""
